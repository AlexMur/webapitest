﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApiTest.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class GuidController : ControllerBase
	{
		// GET api/values
		[HttpGet]
		public ActionResult<Guid> Get()
		{
			return Guid.NewGuid();
		}
	}
}
